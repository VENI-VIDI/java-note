import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.util.Arrays;

public class SSS {
    public static void main(String[] args) {
        String str1 = "";
        StringBuffer str2 = new StringBuffer("");
        StringBuilder str3 = new StringBuilder("");
        long start = System.currentTimeMillis();
        for (int i = 0; i < 20000; i++) {
            str1 += i;
        }
        long end = System.currentTimeMillis();
        System.out.println(end - start);
        start = System.currentTimeMillis();
        for (int i = 0; i < 2000000; i++) {
            str2.append(i);
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);
        start = System.currentTimeMillis();
        for (int i = 0; i < 200000; i++) {
            str3.append(i);
        }
        end = System.currentTimeMillis();
        System.out.println(end - start);
        System.out.println(str1.equals(str2.toString())+" "+str1.equals(str3.toString()) +" "+str3.toString().equals(str2.toString()));

    }


    /**
     * String 、 byte[] 转换
     * @throws UnsupportedEncodingException
     */
    @Test
    public void test1() throws UnsupportedEncodingException {
        String str = "...";
        byte[] bytes = str.getBytes();
        System.out.println(Arrays.toString(bytes));

        byte[] gbk = str.getBytes("gbk");
        System.out.println(Arrays.toString(gbk));

        String st = new String(bytes);
        String tr = new String(gbk);
        System.out.println(st+"\n"+tr);
    }

    @Test
    public void test2(){
        System.out.println("{\"content\":\""+1+"\",\"msg\":\""+2+"\",\"status\":"+3+",\"date\":\""+4+"\"}");
        LocalDate date = LocalDate.now();
        System.out.println(date.toString());
    }

    @Test
    public void test3(){
        StringBuilder sb = new StringBuilder("[");
        sb.append("sadassda");
        sb.replace(sb.length()-1,sb.length(),"]");
        System.out.println(sb.toString());
    }
}
