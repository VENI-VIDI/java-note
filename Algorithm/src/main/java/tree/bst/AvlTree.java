package tree.bst;

import tree.TreeNode;
import tree.TreeUtil;

/**
 * 增删改查：O(log n)
 * @author zxs
 * @time 2021-07-05-9:38
 */

public class AvlTree {

    private TreeNode root;
    private int size;

    public AvlTree(TreeNode root) {
        this.root = root;
        size = 1;
    }

    public AvlTree() {
        size = 0;
    }


    public int size() {
        return size;
    }

    /**
     * node为失去平衡的节点
     * @param node
     */
    public TreeNode leftRotate(TreeNode node){
        TreeNode target = node.right;
        TreeNode temp = target.left;
        target.left = node;
        node.right = temp;

        /*更新节点高度记录*/
        node.height = getHeight(node);
        target.height = getHeight(target);

        return target;
    }

    /**
     * node为失去平衡的节点
     * @param node
     */
    public TreeNode rightRotate(TreeNode node){
        TreeNode target = node.left;
        TreeNode temp = target.right;
        target.right = node;
        node.left = temp;

        /*更新节点高度记录*/
        node.height = getHeight(node);
        target.height = getHeight(target);

        return target;
    }

    /**
     * 一样的，不允许重复元素
     * @param value
     * @return
     */
    public AvlTree add(int value){
        if(!this.contains(value)){
            root = add(root,value);
        }
        return this;
    }

    /**
     * 添加元素
     *
     * 1、添加
     * 2、检查是否平衡
     * 3、旋转
     *
     * @param node
     * @param value
     * @return
     */
    private TreeNode add(TreeNode node, int value) {
        if(node == null){
            size++;
            return new TreeNode(value);
        }
        if(node.value > value){
            node.left = add(node.left,value);
        }else if(node.value < value){
            node.right = add(node.right,value);
        }
        node.height = getHeight(node);

        /**
         * 判断是否平衡，若不平衡判断其为那种类型
         * 1、LL
         * 2、LR
         * 3、RR
         * 4、RL
         */
        int balance = getBalanceKey(node);

        if(balance > 1 && getBalanceKey(node.left) >= 0){
            return rightRotate(node);
        }
        if(balance > 1 && getBalanceKey(node.left) < 0){
            node.left = leftRotate(node.left);
            return rightRotate(node);
        }
        if(balance < -1 && getBalanceKey(node.right) <= 0){
            return leftRotate(node);
        }
        if(balance < -1 && getBalanceKey(node.right) > 0){
            node.right = rightRotate(node.right);
            return leftRotate(node);
        }
        return node;
    }

    /**
     * 删除
     * @param value
     */
    public void remove(int value){
        root = remove(root,value);
    }

    private TreeNode remove(TreeNode node, int value) {
        if(node == null){
            return null;
        }
        TreeNode result = null;
        if(node.value > value){
            node.left = remove(node.left,value);
            return node;
        }else if(node.value < value){
            node.right = remove(node.right,value);
            result = node;
        }else{
            if(node.left == null){
                TreeNode right = node.right;
                node.right = null;
                size--;
                result = right;
            }else if(node.right == null){
                TreeNode left = node.left;
                node.left = null;
                size--;
                return left;
            }else {
                TreeNode temp = findMin(node.right);
                temp.right = remove(node.right,value);
                temp.left = node.left;
                result = temp;
            }
        }

        if(result == null){
            return null;
        }

        result.height = 1 + getHeight(result);

        /*判断是否平衡，若不平衡判断其为那种类型
         * 1、LL
         * 2、LR
         * 3、RR
         * 4、RL
         * */
        int balance = getBalanceKey(result);

        if(balance > 1 && getBalanceKey(result.left) >= 0){
            return rightRotate(result);
        }
        if(balance > 1 && getBalanceKey(result.left) < 0){
            result.left = leftRotate(result.left);
            return rightRotate(result);
        }
        if(balance < -1 && getBalanceKey(result.right) <= 0){
            return leftRotate(result);
        }
        if(balance < -1 && getBalanceKey(result.right) > 0){
            result.right = rightRotate(result.right);
            return leftRotate(result);
        }
        return result;

    }

    /**
     * 查找
     * @param value
     * @return
     */
    public boolean contains(int value) {
        return find(root, value) == null ? false : true;
    }

    private TreeNode find(TreeNode node, int value) {
        try{
            if(node.value > value){
                return find(node.left, value);
            }
            else if (node.value < value){
                return find(node.right, value);
            }else{
                return node;
            }
        }catch (Exception e){
            return null;
        }
    }

    private TreeNode findMin(TreeNode node) {
        if(node.left == null){
            return node;
        }
        return findMin(node.left);
    }

    private TreeNode findMax(TreeNode node) {
        if(node.right == null){
            return node;
        }
        return findMax(node.right);
    }

    public int getHeight(){
        return this.getHeight(root);
    }

    public int getHeight(TreeNode node){
        return TreeUtil.treeHeight(node);
    }


    public int getBalanceKey(TreeNode node){
        if(node == null){
            return 0;
        }
        return getHeight(node.left) - getHeight(node.right);
    }

    public void inOrder(){
        TreeUtil.inOrderStack(root);
    }
}
