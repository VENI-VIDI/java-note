package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Gui {
    private long role_id;
    private String gui_name;
    private int ori;

    private List<Command> commands;
}
