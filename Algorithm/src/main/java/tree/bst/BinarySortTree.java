package tree.bst;

import tree.TreeNode;
import tree.TreeUtil;

/**
 * 二叉排序树
 * 增删改查：O(log n ~ n)
 * @author zxs
 * @time 2021-07-05-9:07
 */

public class BinarySortTree {

    /*根节点*/
    private TreeNode root;
    /*长度*/
    private int size;

    /**
     * 创建一个有根节点的树
     * @param root
     */
    public BinarySortTree(TreeNode root) {
        setRoot(root);
        size = 1;
    }

    /**
     * 创建一个没有根节点的树
     */
    public BinarySortTree(){
        size = 0;
    }

    /**
     * 设置根节点
     * @param root
     */
    public void setRoot(TreeNode root) {
        this.root = root;
    }

    /**
     * 获取根节点
     * @return
     */
    public TreeNode getRoot() {
        return this.root;
    }

    /**
     * 添加节点，方便链式添加，返回值为本身
     * @param value
     * @return
     */
    public BinarySortTree add(int value) {
        if(this.contains(value)){
            return this;
        }
        return add(root, value);
    }

    /**
     * 添加节点的具体方法
     * @param node
     * @param value
     * @return
     */
    private BinarySortTree add(TreeNode node, int value) {
        if (node.value > value) {
            if (node.left == null){
                node.left = new TreeNode(value);
            } else{
                return add(node.left, value);
            }
        } else if (node.value < value) {
            if(node.right == null){
                node.right = new TreeNode(value);
            } else{
                return add(node.right, value);
            }
        }
        size++;
        return this;
    }

    /**
     * 删除
     * @param value
     */
    public void remove(int value){
        root = remove(root,value);
    }

    private TreeNode remove(TreeNode node, int value) {
        if(node == null){
            return null;
        }
        if(node.value > value){
            node.left = remove(node.left,value);
            return node;
        }else if(node.value < value){
            node.right = remove(node.right,value);
            return node;
        }else{
            if(node.left == null){
                TreeNode right = node.right;
                node.right = null;
                size--;
                return right;
            }
            if(node.right == null){
                TreeNode left = node.left;
                node.left = null;
                size--;
                return left;
            }
            TreeNode result = findMin(node.right);
            result.right = remove(node.right, result.value);
            result.left = node.left;
            return result;
        }
    }

    /**
     * 查找
     * @param value
     * @return
     */
    public boolean contains(int value) {
        return find(root, value) == null ? false : true;
    }

    private TreeNode find(TreeNode node, int value) {
        try {
            if (node.value > value) return find(node.left, value);
            else if (node.value < value) return find(node.right, value);
            else return node;
        }catch (Exception e){
            return null;
        }
    }

    /**
     * 获取该树的最大值
     * @return
     */
    public int getMax(){
        return findMax(root).value;
    }

    /**
     * 获取该树的最小值
     * @return
     */
    public int getMin(){
        return findMin(root).value;
    }

    private TreeNode findMin(TreeNode node) {
        if(node.left == null) return node;
        return findMin(node.left);
    }

    private TreeNode findMax(TreeNode node) {
        if(node.right == null) return node;
        return findMax(node.right);
    }

    /**
     * 获取该树的节点数量
     * @return
     */
    public int size(){
        return this.size;
    }

    /**
     * 是否为空
     * @return
     */
    public boolean isEmpty(){
        return size == 0;
    }

    /**
     * 中序遍历
     */
    public void inOrder(){
        TreeUtil.inOrderStack(root);
    }

    public int height(){
        return TreeUtil.treeHeight(root);
    }

}
