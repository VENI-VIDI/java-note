package pcc;

public class Custom implements Runnable{

    private Clerk clerk;

    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Custom(int count){
        this.count = count;
    }

    public Clerk getClerk() {
        return clerk;
    }

    public void setClerk(Clerk clerk) {
        this.clerk = clerk;
    }

    @Override
    public void run() {
        while(true){
            synchronized(clerk){
                if(count > 0){
                    int x = clerk.getCount();
                    if(x > 0){
                        this.notify();
                        clerk.setCount(x-1);
                        count--;
                        System.out.println(Thread.currentThread().getName()+":"+count);
                    }else{
                        try {
                            this.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }else {
                    clerk.setCount(20);
                    break;
                }
            }
        }
    }
}
