import org.junit.Test;

import java.util.ArrayList;

public class ArrayListTest {

    @Test
    public void test1(){
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(3);
        System.out.println("1:"+list.toString());
        list.add(1,3);
        System.out.println("2:"+list.toString());
        list.remove(0);
        System.out.println("3:"+list.toString());
        list.remove(new Integer(3));
        System.out.println("4:"+list.toString());
        list.add(2);
        list.add(3);
        System.out.println("5:"+list.toString());
        Integer x = new Integer(3);
        list.remove(x);
        System.out.println("6:"+list.toString());
        list.set(1,1);
        System.out.println("7:"+list.toString());
    }
}
