package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/demo")
public class servlet1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] cookies = req.getCookies();
        for(Cookie cookie:cookies) System.out.println(cookie.getName()+" "+cookie.getValue());

        HttpSession session = req.getSession();
        Cookie cookie = new Cookie("JSESSIONID",session.getId());
        cookie.setMaxAge(100);
        resp.addCookie(cookie);
        System.out.println(session);
        System.out.println(session.getAttribute("zxs"));
        System.out.println(session.getId());

//        JSESSIONID 15848F2B04F2DE5B8AA0DA9CB7F2D8E2
//        org.apache.catalina.session.StandardSessionFacade@4ddb19fd
//        JSESSIONID 1006F4BF0A25F7B9BA6F2EE13871895C
//        org.apache.catalina.session.StandardSessionFacade@618db6af

    }
}
