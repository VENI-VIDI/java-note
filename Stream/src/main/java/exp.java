import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class exp {
    public static void main(String[] args) {
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
        list1.add(4);
        list1.add(5);
        list1.add(6);
        ArrayList<Integer> list2 = new ArrayList<>();
        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        list2.add(5);
        list2.add(6);
        list2.add(7);
        Stream<Integer> stream1 = list1.stream().filter(i->i%2==1).limit(2);
        Stream<Integer> stream2 = list1.stream().filter(i->i%2==0).skip(2);
        Stream.concat(stream1,stream2).map(i->new Person(i)).forEach(p-> System.out.println(p));
    }
}
