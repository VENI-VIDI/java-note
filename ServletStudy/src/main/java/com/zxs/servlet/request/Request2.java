package com.zxs.servlet.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;


/**
 * 获取请求头
 */
@WebServlet("/request2")
public class Request2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Enumeration<String> headerNames = req.getHeaderNames();
        while(headerNames.hasMoreElements()){
            String name = headerNames.nextElement();
            System.out.println(name+"---"+req.getHeader(name));
        }
        System.out.println("=============================");
        /*获取请求头数据
         * 查看来源
         * */
        System.out.println(req.getHeader("referer"));
    }
}
