package session;

public class testFunIn {
    public static void main(String[] args) {
        test(str->System.out.println(str));
        test(System.out::println);
    }

    private static void test(FunIn funIn){
        funIn.print("IntoYXL");
    }
}
