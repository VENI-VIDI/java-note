package demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/demo1")
public class Cookie2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        Cookie[] cookies = req.getCookies();
        String pr = "";
        for(Cookie cookie:cookies){
            if(cookie.getName().equals("lastTime")){
                pr = cookie.getValue();
            }
            System.out.println(cookie.getName()+" "+cookie.getValue());
        }
        PrintWriter writer = resp.getWriter();
        if(pr.length()==0) pr+="你是个傻逼";
        writer.print(pr);
    }
}
