package zxs.dao;

import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import zxs.entity.User;

@Repository
public interface UserDao {

    /**
     * 按id查询
     * @param name
     * @return
     */
    @Select("select * from user where name = #{userName}")
    User selectByName(String name);
}
