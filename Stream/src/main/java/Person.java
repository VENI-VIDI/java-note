import java.io.Serializable;

public class Person implements Serializable {
    private Integer ID;

    @Override
    public String toString() {
        return "Person{" +
                "ID=" + ID +
                '}';
    }

    public Person(Integer ID) {
        this.ID = ID;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }
}
