package demo;

import com.jsoniter.JsonIterator;
import com.jsoniter.any.Any;
import entity.Role;
import entity.RoleInUser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class JsoniterToJson {
    public static void main(String[] args) {
//        Any obj = JsonIterator.deserialize("[1,2,3]");
//        System.out.println(obj.toString());
//        System.out.println(System.currentTimeMillis());
        Any any = JsonIterator.deserialize("[{\"role_id\":1,\"role_name\":\"不工人员\",\"guis\":[{\"role_id\":1,\"gui_name\":\"a\",\"ori\":1,\"commands\":[{\"role_id\":1,\"gui_name\":\"a\",\"command_name\":\"aA\",\"ui_name\":\"a@aA\"}]}]}]");
        System.out.println(any.get(0).toString("role_name"));
//        System.out.println(System.currentTimeMillis());
//        System.out.println(any.toString("role_id","role_name"));
//        Any an = any.get("role_id","role_name");
//        System.out.println(an.toString());
//        System.out.println(any.get("role_id"));
//        Any any2 = any.get("guis");
//        System.out.println(any2);
//        for(Any an : any2){
//            System.out.println("an         " + an);
//            System.out.println("role_id  " + an.toInt("role_id"));
//        }
//
//        System.out.println(any.toLong("role_id") + Integer.MAX_VALUE);
//        long a = any.toLong("role_id");
//        String str = any.toString("role_name");
//        System.out.println("a = "+a);
//        System.out.println("user_name = " + str);

//        String str = "[{'id':'123','name':'zxs','guis':[{'role_id':'123','gui_name':'asda','commands':[{'role_id':'123','gui_name':'zxs',command_name':'eeee','ui_name':'asdasda'}],'ordinal':'4'}],'action':'I'}]";
//        int index = 0;
//        String res = "";
//        while(index < str.length()){
//            if(str.charAt(index) == '\'') res += '\"';
//            else res += str.charAt(index);
//            index++;
//        }
//        Any obj = JsonIterator.deserialize(res);
//        System.out.println(obj.get("guis") == null);
////        System.out.println(obj.get("id"));

//        List<String> list = new ArrayList<>();
//        list.add("a");
//        list.add("b");
//        list.add("b");
//        list.add("c");
//        System.out.println(list.indexOf("c"));
    }
}
