package excel.poi_stream;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.openxml4j.opc.internal.ContentTypeManager;
import org.apache.poi.xssf.eventusermodel.XSSFBReader;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.junit.Test;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author zxs
 * @time 2021-08-09-9:18
 */

public class DEMO {
    public static void main(String[] args) throws OpenXML4JException, IOException {
        OPCPackage pkg = OPCPackage.open("D:/test.xlsx",PackageAccess.READ);
        XSSFBReader reader = new XSSFBReader(pkg);
    }

    @Test
    public void test() throws InvalidFormatException, IOException {
        String file_path = "D:/test.xlsx";
        OPCPackage pkg =OPCPackage.open(file_path, PackageAccess.READ);
        try {
            FileInputStream fis = new FileInputStream(file_path);
            XSSFReader reader = new XSSFReader(pkg);

//        获取sharedStrings.xml,其中存放的excel的字符
            SharedStringsTable sst = reader.getSharedStringsTable();

//        SAX解析xml
            StylesTable st = reader.getStylesTable();
            XMLReader xml_reader = XMLReaderFactory.createXMLReader();
            xml_reader.setContentHandler(new SheetHandle());
            XSSFReader.SheetIterator sheet = (XSSFReader.SheetIterator) reader.getSheetsData();
            while(sheet.hasNext()){
                InputStream sheet_stream = sheet.next();
                InputSource sheet_source = new InputSource(sheet_stream);
                try {
                    xml_reader.parse(sheet_source);
                }catch (Exception e){

                }finally {
                    sheet_stream.close();
                }
            }
        } catch (SAXException | FileNotFoundException e) {
            e.printStackTrace();
        } catch (OpenXML4JException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            pkg.close();
        }
    }
}
