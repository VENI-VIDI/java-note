package pcc;

public class Clerk {

    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Clerk(int count){
        this.count=count;
    }
}
