package adt;

/**
 * @author zxs
 * @time 2021-07-19-11:38
 */

public class MyLinkedList {

    private Node head;
    private int size;

    public MyLinkedList(){}

    public MyLinkedList(Node head){
        this.head = head;
        size = 0;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public MyLinkedList add(int value){
        head = add(head,value);
        size++;
        return this;
    }

    private Node add(Node node, int value) {
        if(node == null) return new Node(value);
        node.next = add(node.next,value);
        return node;
    }

    public MyLinkedList addNode(Node node){
        head = addNode(head,node);
        size++;
        return this;
    }

    private Node addNode(Node head, Node node) {
        if(head == null) return node;
        head.next = addNode(head.next,node);
        return head;
    }

    public void delete(int value){
        head = delete(head,value);
    }

    private Node delete(Node head, int value) {
        Node node = new Node(0,head);
        Node temp = node.next;
        while(temp != null){
            if(temp.value == value){
                if(temp.next != null) {
                    temp.value = temp.next.value;
                    temp.next = temp.next.next;
                }
            }
            temp = temp.next;
        }
        return node.next;
    }

    public void update(int target,int value){
        head = update(head,target,value);
    }

    private Node update(Node node, int target, int value) {
        if(node == null) return null;
        if(node.value == target) node.value = value;
        else node.next = update(node.next,target,value);
        return node;
    }

    public int get(int index){
        Node node = get(head, index);
        return node == null ? Integer.MIN_VALUE : node.value;
    }

    private Node get(Node node, int index) {
        if(node == null) return null;
        if(index == 0) return node;
        return get(node.next,index-1);
    }

    @Override
    public String toString() {
        if(size == 0) return "[]";
        StringBuilder sb = new StringBuilder("[");
        Node node = new Node(0,head);
        while(node.next != null){
            node = node.next;
            sb.append(node.value).append(",");
        }
        sb.replace(sb.length()-1,sb.length(),"]");
        return sb.toString();
    }
}

class Node{

    public int value;
    public Node next;

    public Node(){}

    public Node(int value){
        this.value = value;
    }

    public Node(int value,Node node){
        this.value = value;
        this.next = node;
    }
}
