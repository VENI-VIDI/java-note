package com.zxs.servlet.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Set;

/**
 * 获取请求参数
 */

@WebServlet("/request3")
public class Request3 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        获取参数列表


//        Enumeration<String> parameterNames = req.getParameterNames();
//        while(parameterNames.hasMoreElements()){
//            String name = parameterNames.nextElement();
//            System.out.println(name + " " + req.getParameter(name));
//        }
//        String[] aa = req.getParameterValues("aa");
//        for(String s:aa) System.out.println(s);

        Map<String,String[]> parameterMap = req.getParameterMap();
        Set<String> set = parameterMap.keySet();
        for(String s:set) System.out.println(s+" "+req.getParameter(s));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doGet(req, resp);
    }
}
