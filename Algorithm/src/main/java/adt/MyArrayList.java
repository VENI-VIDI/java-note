package adt;

import java.util.Arrays;

/**
 * @author zxs
 * @time 2021-07-19-10:42
 */

public class MyArrayList {
    private int[] arr;
    private int size;

    public MyArrayList() {
        arr = new int[10];
        size = 0;
    }

    public MyArrayList add(int value){
        if(size < arr.length){
            arr = newArr();
        }
        arr[size++] = value;
        return this;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public int size(){
        return size;
    }

    public void clear(){
        arr = new int[10];
    }

    private int[] newArr(){
        int[] newArr = new int[arr.length*2];
        System.arraycopy(arr, 0, newArr, 0, arr.length);
        return newArr;
    }

    public void deleteByValue(int value){
        int offset = 0;
        int index = 0;
        while(index + offset < size){
            arr[index] = arr[index+offset];
            if(arr[index] == value)offset++;
            else index++;
        }
        size -= offset;
    }

    public boolean deleteByIndex(int index){
        if(index >= size) return false;
        for(int i = index; i < size-1; i++){
            arr[i] = arr[++index];
        }
        size--;
        return true;
    }

    public boolean update(int index,int value){
        if(index >= size) return false;
        arr[index] = value;
        return true;
    }

    public int get(int index){
        if(index >= size) return Integer.MIN_VALUE;
        return arr[index];
    }

    public int index(int value){
        int index = -1;
        for (int i = 0; i < size; i++) {
            if(arr[i] == value){
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public String toString() {
        if(size ==0) return "[]";
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            sb.append(arr[i]).append(",");
        }
        sb.replace(sb.length()-1,sb.length(),"]");
        return sb.toString();
    }
}
