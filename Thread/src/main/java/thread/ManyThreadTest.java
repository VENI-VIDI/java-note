package thread;


class MyThreads extends Thread{

    private static int target = 100;

    @Override
    public void run() {
        while(target > 0){
            System.out.println(getName()+":余票"+target);
            target--;
        }
    }
}

public class ManyThreadTest {
    public static void main(String[] args) {
        MyThreads thread1 = new MyThreads();
        MyThreads thread2 = new MyThreads();
        MyThreads thread3 = new MyThreads();
        thread1.setName("1");
        thread2.setName("2");
        thread3.setName("3");
        thread1.start();
        thread2.start();
        thread3.start();
    }
}
