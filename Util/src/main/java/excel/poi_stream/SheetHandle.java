package excel.poi_stream;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.OpenXML4JException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.junit.Test;
import org.xml.sax.*;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zxs
 * @time 2021-08-09-10:35
 */

public class SheetHandle extends DefaultHandler {

    enum data_type{
        BOOL, ERROR, FORMULA, INLINESTR, SSTINDEX, NUMBER, DATE, NULL
    }

    Connection connection = null;
    PreparedStatement ps = null;

    SharedStringsTable sst = null;
    StylesTable st = null;
    data_type type = data_type.SSTINDEX;

    int count = 0;
    int rowIndex = -1;
    int cellIndex = 0;
    int columnLen = 0;
    boolean is_TElement = false;

    String value = null;
    StringBuilder sb = new StringBuilder("");


    @Test
    public void test() throws InvalidFormatException, IOException {
        String file_path = "D:/new-1628494614611.xlsx";
//        String file_path = "D:/test.xlsx";
        OPCPackage pkg = OPCPackage.open(file_path, PackageAccess.READ);
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            connection = DriverManager.getConnection("jdbc:sqlserver://127.0.0.1;DatabaseName=ORAPS_SCSS_COMPANY_YIPU2", "sa", "Oraps123");
//            connection = DriverManager.getConnection("");
            FileInputStream fis = new FileInputStream(file_path);
            XSSFReader reader = new XSSFReader(pkg);

//        获取sharedStrings.xml,其中存放的excel的字符
            sst = reader.getSharedStringsTable();

//        SAX解析xml
            st = reader.getStylesTable();
            XMLReader xml_reader = XMLReaderFactory.createXMLReader();
            xml_reader.setContentHandler(this);
            XSSFReader.SheetIterator sheet = (XSSFReader.SheetIterator) reader.getSheetsData();
            while (sheet.hasNext()) {
                InputStream sheet_stream = sheet.next();
                InputSource sheet_source = new InputSource(sheet_stream);
                try {
                    xml_reader.parse(sheet_source);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                } finally {
                    sheet_stream.close();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        } finally {
            pkg.close();
        }
    }


    @Override
    public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
        if ("row".equals(name)) rowIndex++;
        if("t".equals(name)){
            is_TElement = true;
        }else is_TElement = false;
        value = "";
//        System.out.println(name + attributes.getValue("t"));
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        value = new String(ch, start, length);
    }

    @Override
    public void endElement(String uri, String localName, String name) throws SAXException {
        if ("row".equals(name)) {
            sb.replace(sb.length()-1,sb.length(),")");
            try {
                if(rowIndex > 1) {
                    ps = connection.prepareStatement(sb.toString());
                    ps.executeQuery();
                    sb = new StringBuilder();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            if(!"".equals(sb.toString()) || !",".equals(sb.toString())) count++;
            cellIndex = 0;
        } else if ("worksheet".equals(name)) {
            System.out.println(count);
            System.out.println("end");
        }else if("c".equals(name)){
            sb.append(value).append(",");
        }
    }

    public void setType(Attributes attributes){
        String cellType = attributes.getValue("t");
        if ("b".equals(cellType)) { //处理布尔值
            type = data_type.BOOL;
        } else if ("e".equals(cellType)) {  //处理错误
            type = data_type.ERROR;
        } else if ("inlineStr".equals(cellType)) {
            type = data_type.INLINESTR;
        } else if ("s".equals(cellType)) { //处理字符串
            type = data_type.SSTINDEX;
        } else if ("str".equals(cellType)) {
            type = data_type.FORMULA;
        }
    }

    public String getValue(String value,String thisStr){
        switch (type) {
            // 这几个的顺序不能随便交换，交换了很可能会导致数据错误
            case BOOL: //布尔值
                char first = value.charAt(0);
                thisStr = first == '0' ? "FALSE" : "TRUE";
                break;
            case ERROR: //错误
                thisStr = "\"ERROR:" + value.toString() + '"';
                break;
            case FORMULA: //公式
                thisStr = '"' + value.toString() + '"';
                break;
            case INLINESTR:
                XSSFRichTextString rtsi = new XSSFRichTextString(value.toString());
                thisStr = rtsi.toString();
                rtsi = null;
                break;
            case SSTINDEX: //字符串
                String sstIndex = value.toString();
                try {
                    int idx = Integer.parseInt(sstIndex);
                    XSSFRichTextString rtss = new XSSFRichTextString(sst.getEntryAt(idx));//根据idx索引值获取内容值
                    thisStr = rtss.toString();
                    //System.out.println(thisStr);
                    //有些字符串是文本格式的，但内容却是日期

                    rtss = null;
                } catch (NumberFormatException ex) {
                    thisStr = value.toString();
                }
                break;
            case NUMBER: //数字
                thisStr = value;
                thisStr = thisStr.replace("_", "").trim();
                break;
            default:
                thisStr = " ";
                break;
        }
        return thisStr;
    }

    @Test
    public void ts(){
        String str = "123456789";
        System.out.println(str.substring(5));
    }
}
