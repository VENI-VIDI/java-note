package tree;

/**
 * @author zxs
 * @time 2021-07-02-16:22
 */

public class TreeNode {
    public int value;
    public TreeNode left;
    public TreeNode right;
    public int height;

    public TreeNode(int value, TreeNode left, TreeNode right) {
        this.value = value;
        this.left = left;
        this.right = right;
        this.height = 1;
    }

    public TreeNode(int value) {
        this.value = value;
    }

    public TreeNode() {
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
}
