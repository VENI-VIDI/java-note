package excel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author zxs
 * @time 2021-07-23-9:14
 */

public class ExcelWrite {
    public static void main(String[] args) throws IOException {
        long start = System.currentTimeMillis();
        writeExcel("D:/test_three_type.xlsx");
        long end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    @Test
    public void test() throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class cls = Class.forName("java.util.Collection");
        Object obj = cls.getConstructor().newInstance();
        System.out.println(obj);
    }

    /**
     * 对excel的写操作
     * @param filePath
     * @throws IOException
     */
    public static void writeExcel(String filePath) throws IOException {
        OutputStream fos = null;
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = null;
        Row row = null;
        Cell cell = null;
        try {
            fos = new FileOutputStream(filePath);
            sheet = workbook.createSheet("et");
            for(int i = 0; i < 10000; i++){
                row = sheet.createRow(i);
                for(int j = 0; j < 20; j++){
                    cell = row.createCell(j);
                    cell.setCellType(CellType.STRING);
                    cell.setCellValue(i+j);
                }
            }
            workbook.write(fos);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }finally {
            fos.close();
        }
    }


    @Test
    public void aka(){
        OutputStream fos = null;
        Workbook workbook = null;
        try{
            fos = new FileOutputStream("D:/aaa.xlsx");
            workbook = new XSSFWorkbook();
            workbook.createSheet("aaaa");
            workbook.write(fos);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void tt(){
        OutputStream fos = null;
        Workbook workbook = null;
        try {
            fos = new FileOutputStream("D:/aaa.xlsx");
            workbook = new SXSSFWorkbook(1000);
            workbook.createSheet("test");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
