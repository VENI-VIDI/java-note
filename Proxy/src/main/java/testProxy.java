import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class testProxy {
    public static void main(String[] args) {
        Phone phone = new Phone();
        Sale sale = (Sale) Proxy.newProxyInstance(phone.getClass().getClassLoader(), phone.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                if (method.getName().equals("sale")) {
                    double money = (double) args[0];
                    System.out.println("送手机壳");
                    String obj = (String) method.invoke(phone, money * 0.8);
                    System.out.println("包邮");
                    return obj + "碎屏险";
                } else {
                    Object obj = method.invoke(phone, args);
                    return obj;
                }
            }
        });

        sale.sale(4000);
        sale.show();

    }
}
