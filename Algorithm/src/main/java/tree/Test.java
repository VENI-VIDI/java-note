package tree;

/**
 * @author zxs
 * @time 2021-07-02-16:31
 */

public class Test {
    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(3);
        root.left.right = new TreeNode(4);
        root.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        root.right.right.right = new TreeNode(8);

        long start,end;

        /*前序遍历--递归*/
        start = System.currentTimeMillis();
        TreeUtil.preOrder(root);
        end = System.currentTimeMillis();
        System.out.println("\n"+(end - start));

        /*前序遍历--迭代*/
        start = System.currentTimeMillis();
        TreeUtil.preOrderStack(root);
        end = System.currentTimeMillis();
        System.out.println("\n"+(end - start));


        /*中序遍历--递归*/
        start = System.currentTimeMillis();
        TreeUtil.inOrder(root);
        end = System.currentTimeMillis();
        System.out.println("\n"+(end - start));

        /*中序遍历--迭代*/
        start = System.currentTimeMillis();
        TreeUtil.inOrderStack(root);
        end = System.currentTimeMillis();
        System.out.println("\n"+(end - start));


        /*后序遍历--递归*/
        start = System.currentTimeMillis();
        TreeUtil.postOrder(root);
        end = System.currentTimeMillis();
        System.out.println("\n"+(end - start));

        /*后序遍历--迭代*/
        start = System.currentTimeMillis();
        TreeUtil.postOrderStack1(root);
        end = System.currentTimeMillis();
        System.out.println((end - start));

        /*二叉树高度*/
        int height = TreeUtil.treeHeight(root);
        int leftHeight = TreeUtil.treeLeftHeight(root);
        int rightHeight = TreeUtil.treeRightHeight(root);
        System.out.println(height);
        System.out.println(leftHeight);
        System.out.println(rightHeight);
    }
}
