package demo02;

import java.util.ArrayList;
import java.util.function.Predicate;

public class exp {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        String[] strings = {"yxl","zxs","jjj","zz"};
        for(String str:strings){
            if(pd(str,(st)->{
                return st.length()>2;
            },(st)->{
                return str.contains("x");
            })){
                list.add(str);
            }
        }
        System.out.println(list);
    }

    private static boolean pd(String str, Predicate<String> predicate1,Predicate<String> predicate2){
        return predicate1.and(predicate2).test(str);
    }
}
