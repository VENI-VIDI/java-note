package SqlServer;

import java.sql.*;

public class SqlServerLink {
    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        String url = "jdbc:sqlserver//localhost:1433;databasename=ORAPS_SCSS_COMPANY_APTIV_2";
        String username = "sa";
        String password = "Oraps123";
        Connection connection = DriverManager.getConnection(url,username,password);
        PreparedStatement ps = connection.prepareStatement("select * from user");
        ResultSet set = ps.executeQuery();
        while(set.next()){
            System.out.println(set.getString("code"));
        }
        set.close();
        ps.close();
        connection.close();
    }
}
