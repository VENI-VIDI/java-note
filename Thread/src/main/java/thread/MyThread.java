package thread;

public class MyThread extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName()+"加载进度:"+ (i%2==0?0:i));
        }
    }
}


class MyThreadTest{
    public static void main(String[] args) {
        MyThread thread = new MyThread();
        thread.setName("线程1");
        thread.start();
        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName()+"加载进度:"+(i%2==1?1:i));
        }
    }
}
