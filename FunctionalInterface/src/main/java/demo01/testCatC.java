package demo01;

import java.util.function.Consumer;

public class testCatC {
    public static void main(String[] args) {
        CC("yxl",
                (y) -> {
                    System.out.println(y.charAt(0));
                }, (xl) -> {
                    System.out.println(xl.substring(1));
                });
    }

    private static void CC(String name, Consumer<String> consumer1, Consumer<String> consumer2) {
//        consumer1.accept(name);
//        consumer2.accept(name);
        /*使用andThen*/
        consumer1.andThen(consumer2).accept(name);
    }
}
