package com.zxs.servlet.request;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/request")
public class RequestStudy extends HttpServlet {

    /**
     * 获取请求行
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//        获取请求方式            String getMethod()
        System.out.println(req.getMethod());
//        获取虚拟目录            String getContextPath();
        System.out.println(req.getContextPath());
//        获取Servlet路径        String getServletPath()
        System.out.println(req.getServletPath());
//        获取get方式请求参数      String getQueryString()
        System.out.println(req.getQueryString());
//        获取请求URI            String getRequestURI()
//        获取请求URL            String getRequestURL()
        System.out.println(req.getRequestURI());
        System.out.println(req.getRequestURL());
//        获取协议及其版本         String getProtocol()
        System.out.println(req.getProtocol());
//        获取客户机IP地址        String getRemoteAddr()
        System.out.println(req.getRemoteAddr());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
