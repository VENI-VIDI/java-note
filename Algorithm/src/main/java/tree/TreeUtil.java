package tree;

import java.util.*;

/**
 * @author zxs
 * @time 2021-07-02-16:25
 */

public class TreeUtil {

    public static void preOrder(TreeNode root){
        if(root == null) return;
        System.out.print(root.value + " ");
        preOrder(root.left);
        preOrder(root.right);
    }

    public static void preOrderStack(TreeNode root){
        if(root == null) return;
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while(!stack.isEmpty()){
            TreeNode node = stack.pop();
            System.out.print(node.value + " ");
            if(node.right != null) stack.push(node.right);
            if(node.left != null) stack.push(node.left);
        }
    }

    public static void inOrder(TreeNode root){
        if(root == null) return;
        inOrder(root.left);
        System.out.print(root.value + " ");
        inOrder(root.right);
    }

    public static void inOrderStack(TreeNode root){
        if(root == null) return;
        Stack<TreeNode> stack = new Stack<>();
        TreeNode node = root;
        while(node != null || !stack.isEmpty()){
            while(node != null){
                stack.push(node);
                node = node.left;
            }
            root = stack.pop();
            System.out.print(root.value + " ");
            node = root.right;
        }
    }

    public static void postOrder(TreeNode root){
        if(root == null) return;
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.value + " ");
    }

    /**
     * list记录
     * @param root
     */
    public static void postOrderStack1(TreeNode root){
        if(root == null) return;
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        List<Integer> list = new ArrayList<>();
        while(!stack.isEmpty()){
            TreeNode node = stack.pop();
            if(node != null){
                list.add(node.value);
                stack.push(node.left);
                stack.push(node.right);
            }
        }
        Collections.reverse(list);
        System.out.println(list.toString());
    }

    /**
     * 双栈遍历
     * @param root
     */
    public static void postOrderStack2(TreeNode root){
        if(root == null) return;
        Stack<TreeNode> stack = new Stack<>(),result = new Stack<>();
        stack.push(root);
        TreeNode node;
        while(!stack.isEmpty()){
            node = stack.pop();
            result.push(node);
            if(node.left != null) stack.push(node.left);
            if(node.right != null) stack.push(node.right);
        }
        while(!result.isEmpty()){
            System.out.println(result.pop().value + " ");
        }
    }

    /**
     * 设置pre节点的后续遍历
     * @param root
     */
    public static void postOrderStack3(TreeNode root){
        TreeNode node,pre = null;
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while(!stack.isEmpty()){
            node = stack.pop();
            if((node.left == null && node.right == null) || (pre != null && (pre == node.left || pre == node.right))){
                System.out.println(node.value);
                stack.pop();
                pre = node;
            }else{
                if(node.right != null) stack.push(node.right);
                if(node.left != null) stack.push(node.left);
            }
        }
    }

    /**
     * 层级遍历
     * @param root
     */
    public void levelOrder(TreeNode root){
        if(root == null) return;
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        while(!queue.isEmpty()){
            TreeNode node = queue.poll();
            System.out.println(node.value + " ");
            if(node.left != null) queue.add(node.left);
            if(node.right != null) queue.add(node.right);
        }
    }

    /**
     * 传统的遍历(递归)：每一个节点都会访问3次
     * 节点如果没有父指针 或 采用的是栈内迭代的方法，其不能回到父节点
     * Morris则是通过叶子节点的空闲指针回到父节点，以此减少了递归、迭代产生的空间使用，将空间复杂度降至 O(1)
     * @param root
     */
    public static void morris(TreeNode root){
        if(root == null) return;
        TreeNode node = root;
        TreeNode temp = null;
        while(node != null){
            temp = node.left;
            if(temp != null){
                while(temp.right != null && temp.right != node) temp = temp.right;
                if(temp.right == null){
                    temp.right = node;
                    node = node.left;
                    continue;
                }else temp.right = null;
            }
            node = node.right;
        }
    }

    public static int treeHeight(TreeNode root){
        return height(root,0);
    }

    private static int height(TreeNode root, int i) {
        if(root == null) return i;
        int left = height(root.left,i+1);
        int right = height(root.right,i+1);
        return left > right ? left : right;
    }

    public static int treeLeftHeight(TreeNode root){
        return treeHeight(root.left) + 1;
    }

    public static int treeRightHeight(TreeNode root){
        return treeHeight(root.right) + 1;
    }

}
