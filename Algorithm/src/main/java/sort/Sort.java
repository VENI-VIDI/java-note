package sort;

import list.MyList;
import org.junit.Test;
import tree.TreeNode;
import tree.bst.AvlTree;
import tree.bst.BinarySortTree;

public class Sort {
    public static void main(String[] args) {
        int[] arr = arr();

//        new Thread(()->{
//            bubbleSort1(arr.clone());
//        }).start();
//        new Thread(()->{
//            selectSort1(arr.clone());
//        }).start();
//        new Thread(()->{
//            bubbleSort2(arr.clone());
//        }).start();
        bubbleSort1(arr.clone());
        selectSort1(arr.clone());
        bubbleSort2(arr.clone());
        bubbleSort3(arr.clone());
        insertSort1(arr.clone());
        insertSort2(arr.clone());
        insertSort3(arr.clone());
        shellSort1(arr.clone());
        shellSort2(arr.clone());
        quickSort1(arr.clone());
        quickSort2(arr.clone());
//        new Thread(()->{
//            quickSort3(arr.clone());
//        }).start();
//        quickSort3(arr.clone());
        int[] x = arr.clone();
        BinarySortTree bst = new BinarySortTree(new TreeNode(arr[0]));
        treeSort1(x,bst);
//        TreeUtil.inOrderStack(tree.getRoot());
        AvlTree avl = new AvlTree();
        treeSort2(x,avl);
        MyList list = new MyList();
//        listSort(x,list);
    }


    private static void listSort(int[] arr, MyList list){
        long start = System.currentTimeMillis();
        for(int i = 1; i < arr.length; i++){
            list.add(arr[i]);
        }
        long end = System.currentTimeMillis();
        System.out.println("listSort ===>> " + (end-start));
    }

    private static void treeSort1(int[] arr,BinarySortTree tree){
        long start = System.currentTimeMillis();
        for(int i = 1; i < arr.length; i++){
            tree.add(arr[i]);
        }
        long end = System.currentTimeMillis();
        System.out.println("treeSort1 ===>> " + (end-start));
    }

    private static void treeSort2(int[] arr, AvlTree tree){
        long start = System.currentTimeMillis();
        for(int i = 1; i < arr.length; i++){
            tree.add(arr[i]);
        }
        long end = System.currentTimeMillis();
        System.out.println("treeSort2 ===>> " + (end-start));
    }

    /**
     * 选择排序
     * @param arr
     */
    private static void selectSort1(int[] arr){
        long start = System.currentTimeMillis();

        for(int i = 0; i< arr.length - 1; i++){
            for(int j = i + 1; j < arr.length; j++){
                if(arr[i] > arr[j]) swap(arr,i,j);
            }
        }

        long end = System.currentTimeMillis();
        System.out.println("selectSort1 ===>> " + (end-start));
//        System.out.println("selectSort1 ===>> " + Arrays.toString(arr));
    }


    /**
     * 基本的冒泡排序
     * @param arr
     */
    private static void bubbleSort1(int[] arr){
        long start = System.currentTimeMillis();

        for(int end = arr.length - 1; end > 0; end--){
            boolean flag = true;
            for(int i = 0; i < end; i++){
                if(arr[i] > arr[i+1]) {
                    swap(arr, i, i + 1);
                    flag = false;
                }
            }
            if(flag) break;
        }

        long end = System.currentTimeMillis();
        System.out.println("bubbleSort1 ===>> " + (end-start));
//        System.out.println("bubbleSort1 ===>> " + Arrays.toString(arr));
    }

    /**
     * 冒泡排序
     * 对上次冒泡做记录
     * @param arr
     */
    private static void bubbleSort2(int[] arr){
        long start = System.currentTimeMillis();

        for(int end = arr.length - 1; end > 0; end--){
            int index = 0;
            for(int i = 0; i < end; i++){
                if(arr[i] > arr[i+1]){
                    swap(arr,i,i+1);
                    index = i+1;
                }
            }
            end = index;
        }

        long end = System.currentTimeMillis();
        System.out.println("bubbleSort2 ===>>" + (end-start));
//        System.out.println("bubbleSort2 ===>>" + Arrays.toString(arr));
    }

    /**
     * 双向冒泡
     * @param arr
     */
    private static void bubbleSort3(int[] arr){
        long start = System.currentTimeMillis();

        int left = 0, right = arr.length - 1;
        while(left < right){
            for(int i = left; i < right; i++){
                if(arr[i] > arr[i+1]) swap(arr,i,i+1);
            }
            right--;
            for(int i = right; i > left; i--){
                if(arr[i] < arr[i-1]) swap(arr,i,i-1);
            }
            left++;
        }

        long end = System.currentTimeMillis();
        System.out.println("bubbleSort3 ===>>" + (end-start));
//        System.out.println("bubbleSort3 ===>>" + Arrays.toString(arr));
    }


    /**
     * 插排1
     * @param arr
     */
    private static void insertSort1(int[] arr){
        long start = System.currentTimeMillis();

        for(int i = 1; i < arr.length; i++){
            int j,k = arr[i];
            for(j = i-1; j >= 0; j--){
                if(k < arr[j]) {
                    arr[j + 1] = arr[j];
                }else break;
            }
            arr[j+1] = k;
        }

        long end = System.currentTimeMillis();
        System.out.println("insertSort1 ===>>" + (end-start));
//        System.out.println("insertSort1 ===>>" + Arrays.toString(arr));
    }

    /**
     * 插排2
     * @param arr
     */
    private static void insertSort2(int[] arr){
        long start = System.currentTimeMillis();

        for(int i = 1; i < arr.length; i++){
            for(int j = i; j > 0 && arr[j] < arr[j-1]; j--){
                if(arr[j] < arr[j-1]) {
                    swap(arr, j, j - 1);
                }else break;
            }
        }

        long end = System.currentTimeMillis();
        System.out.println("insertSort2 ===>>" + (end-start));
//        System.out.println("insertSort2 ===>>" + Arrays.toString(arr));
    }


    /**
     * 二分插排
     * @param arr
     */
    private static void insertSort3(int[] arr){
        long start = System.currentTimeMillis();

        for(int i = 1; i < arr.length; i++){
            int k = arr[i];
            int l = 0, r = i-1;
            while(l <= r){
                int mid = (l + r) / 2;
                if(arr[mid] < k) l = mid+1;
                else r = mid-1;
            }
            for(int j = i-1; j >= l; j--) arr[j+1] = arr[j];
            arr[l] =k;
        }

        long end = System.currentTimeMillis();
        System.out.println("insertSort3 ===>>" + (end-start));
//        System.out.println("insertSort3 ===>>" + Arrays.toString(arr));
    }


    /**
     * 希尔排序    步长2
     * @param arr
     */
    private static void shellSort1(int[] arr){
        long start = System.currentTimeMillis();

        for(int gap = arr.length; gap > 0; gap/=2){
            for(int i = gap; i < arr.length; i++){
                int k = arr[i],j;
                for(j = i - gap; j >= 0; j-=gap){
                    if(k < arr[j]) arr[j + gap] = arr[j];
                    else break;
                }
                arr[j + gap] = k;
            }
        }


        long end = System.currentTimeMillis();
        System.out.println("shellSort1 ===>>" + (end-start));
//        System.out.println("shellSort1 ===>>" + Arrays.toString(arr));
    }

    /**
     * 希尔排序   步长3
     * @param arr
     */
    private static void shellSort2(int[] arr){
        long start = System.currentTimeMillis();
        int gap = 0;
        for( ; gap <= arr.length; gap = gap*3 +1);
        for( ; gap > 0; gap = (gap-1)/3){
            for(int i = gap; i < arr.length; i++){
                int k = arr[i],j;
                for(j = i - gap; j >= 0; j-=gap){
                    if(k < arr[j]) arr[j + gap] = arr[j];
                    else break;
                }
                arr[j + gap] = k;
            }
        }

        long end = System.currentTimeMillis();
        System.out.println("shellSort2 ===>>" + (end-start));
//        System.out.println("shellSort2"+Arrays.toString(arr));
    }


    /**
     * 快排
     * @param arr
     */
    private static void quickSort1(int[] arr){
        long start = System.currentTimeMillis();

        if(arr == null || arr.length <= 1) return;
        quick1_1(arr,0,arr.length-1);

        long end = System.currentTimeMillis();
        System.out.println("quickSort1 ===>> " + (end-start));
//        System.out.println("quickSort1 ===>> " + Arrays.toString(arr));
    }

    /**
     * 随机快排
     * @param arr
     */
    private static void quickSort2(int[] arr){
        long start = System.currentTimeMillis();

        if(arr == null || arr.length <= 1) return;
        quick1_2(arr,0,arr.length-1);

        long end = System.currentTimeMillis();
        System.out.println("quickSort2 ===>> " + (end-start));
//        System.out.println("quickSort1 ===>> " + Arrays.toString(arr));
    }


    /**
     * 三路快排
     * @param arr
     */
    private static void quickSort3(int[] arr){
        long start = System.currentTimeMillis();

        if(arr == null || arr.length < 2) return;
        quick1_3(arr,0,arr.length-1);

        long end = System.currentTimeMillis();
        System.out.println("quickSort3 ===>>" + (end-start));
//        System.out.println("quickSort3 ===>> " + Arrays.toString(arr));
    }


    private static void quick1_1(int[] arr,int l,int r){
        if(l >= r) return;
        int p = quick2_1(arr,l,r);
        quick1_1(arr,l,p-1);
        quick1_1(arr,p+1,r);
    }

    private static void quick1_2(int[] arr,int l,int r){
        if(l >= r) return;

        /*取随机数，降低取到极大极小值的概率*/
        swap(arr,l,l+(int)(Math.random() * (r - l + 1)));

        int p = quick2_1(arr,l,r);
        quick1_2(arr,l,p-1);
        quick1_2(arr,p+1,r);
    }

    private static void quick1_3(int[] arr,int l,int r){
        if(l >= r) return;

        /*取随机数，降低取到极大极小值的概率*/
        swap(arr,l,l+(int)(Math.random() * (r - l + 1)));

        int[] p = quick2_2(arr,l,r);
        quick1_2(arr,l,p[0]-1);
        quick1_2(arr,p[1]+1,r);
    }

    /*划分*/
    private static int quick2_1(int[] arr,int l,int r){
        int k = arr[l];
        int result = l;
        for(int i = l+1;i <= r; i++){
            if(arr[i] < k){
                swap(arr,i,++result);
            }
        }
        swap(arr,result,l);
        return result;
    }

    /*三路划分*/
    private static int[] quick2_2(int[] arr,int l,int r){
        int x = l, y = l-1, z = r;
        int k = arr[l];
        while(x < z){
            if(arr[x] < k){
                swap(arr,++y,x++);
            }else if(arr[x] > k){
                swap(arr,--r,x);
            }else x++;
        }
        swap(arr,z,r);
        return new int[] {y+1,z};
    }

    /**
     * 归并排序
     * @param arr
     */
    private static void mergeSort1(int[] arr){
        long start = System.currentTimeMillis();

        if(arr == null || arr.length <2) return;
        merge(arr,0,arr.length-1);

        long end = System.currentTimeMillis();
        System.out.println("bubbleSort3 ===>>" + (end-start));
    }

    private static void merge(int[] arr, int l, int r) {

    }


    private static void Sort(int[] arr){
        long start = System.currentTimeMillis();

        long end = System.currentTimeMillis();
        System.out.println("bubbleSort3 ===>>" + (end-start));
    }


    private static void swap(int[] arr,int i,int j){
        int t = arr[i];
        arr[i] = arr[j];
        arr[j] = t;
    }


    private static int[] arr(){
        int[] x = new int[30000];
        for(int i = 0; i < 30000; i++){
            x[i] = (int)(Math.random()*30000*Math.PI);
        }
        return x;
    }
    @Test
    public void test(){
        int i;
        for(i = 0; i < 10; ) System.out.print(i++ + " ");
        System.out.println(i);
        for(i = 0; i < 10; ) System.out.print(++i + " ");
        System.out.println(i);
    }
}
