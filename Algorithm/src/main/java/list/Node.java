package list;

/**
 * @author zxs
 * @time 2021-07-06-17:42
 */

public class Node {
    public int value;
    public Node next;

    public Node(){}

    public Node(int value){
        this.value = value;
    }
}
