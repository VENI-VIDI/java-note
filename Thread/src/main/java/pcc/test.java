package pcc;

public class test {
    public static void main(String[] args) {
        Clerk clerk = new Clerk(20);
        Custom custom = new Custom(30);
        Product product = new Product();
        product.setClerk(clerk);
        custom.setClerk(clerk);
        Thread thread1 = new Thread(product);
        Thread thread2 = new Thread(custom);

        thread1.start();
        thread2.start();

    }
}
