package demo02;

import java.util.function.Predicate;

public class testPredicate {
    public static void main(String[] args) {
        System.out.println(test("yxl",(per)->{
            return per.equals("yxl");
        }, (peo)->{
            return peo.compareTo("zxs")<0;
        }));
    }

    private static boolean test(String name, Predicate<String> predicate1,Predicate<String> predicate2){
        return predicate1.and(predicate2).test(name);
    }
}
