package demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/demo")
public class Cookie1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        添加cookie
        Cookie cookie1 = new Cookie("yxl","19990516");
        Cookie cookie2 = new Cookie("zxs","19990123");
        response.addCookie(cookie1);
        response.addCookie(cookie2);
//        设置cookie的作用范围
        cookie1.setPath("/Cookie_war");/*作用在Cookie_war项目下*/
        cookie2.setPath("/");/*作用在tomcat服务器下*/
//        设置cookie的生命周期
        /*
        * 0：删除
        * 负数：关闭浏览器后自动清缓
        * 正数：记录到硬盘上 ** s
        * */
        cookie1.setMaxAge(10000);
        cookie2.setMaxAge(60);

    }
}
