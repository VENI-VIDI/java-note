package adt;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.sql.DatabaseMetaData;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * @author zxs
 * @time 2021-07-19-11:15
 */

public class TestForAdt {
    public static void main(String[] args) throws ClassNotFoundException {
        DatabaseMetaData data = null;
    }

    @Test
    public void myArrayList(){
        MyArrayList list = new MyArrayList();
        list.add(4).add(3).add(4).add(8).add(10);
        list.deleteByIndex(1);
        System.out.println(list.toString());
        list.deleteByValue(4);
        System.out.println(list.toString());
        list.update(1,2);
        System.out.println(list.toString());
        System.out.println(list.get(0));
        System.out.println(list.index(2));
        System.out.println(list.get(78));
    }

    @Test
    public void myLinkedList(){
        MyLinkedList list = new MyLinkedList();
        list.add(4).add(3).add(4).add(8).add(10);
        System.out.println(list.toString());
        list.delete(10);
        System.out.println(list.toString());
    }

}
