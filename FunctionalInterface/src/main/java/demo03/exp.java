package demo03;

import java.util.function.Function;

public class exp {
    public static void main(String[] args) {
        String str = "杨小澜,22";
        test(str,(st)->{
            String[] strings = str.split(",");
            return strings[1];
        },(st)->{
            return Integer.valueOf(st);
        },(st)->{
            return st+100;
        });
    }

    private static void test(String str, Function<String,String> function1,Function<String,Integer> function2,Function<Integer,Integer> function3){
        int res = function1.andThen(function2).andThen(function3).apply(str);
        System.out.println(res);
    }
}
