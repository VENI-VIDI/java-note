package list;

/**
 * @author zxs
 * @time 2021-07-06-17:44
 */

public class MyList {

    private Node head;
    private int size;

    public MyList(){
        head = null;
        size = 0;
    }

    public MyList(Node head){
        this.head = head;
        size = 1;
    }

    public int size(){
        return size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public MyList add(int value){
        size++;
        head = add(head,value);
        return this;
    }

    public Node add(Node node,int value){
        if(node == null){
            return new Node(value);
        }
        if(node.value < value){
            node.next = add(node.next,value);
        }else{
            int key = node.value;
            node.value = value;
            node.next = add(node.next,key);
        }
        return node;
    }

    public boolean contains(int value){
        Node node = head;
        while(node != null){
            if(node.value == value) return true;
            node = node.next;
        }
        return false;
    }

    public boolean remove(int value){
        Node node = head;
        while(node != null){
            if(node.value == value){
                node.value = node.next.value;
                node.next = node.next.next;
                size--;
                return true;
            }
            node = node.next;
        }
        return false;
    }

    public boolean removeAll(int value){
        boolean flag = true;
        while(flag){
            flag = remove(value);
        }
        return true;
    }

    public boolean update(int value,int target){
        add(target);
        return remove(value);
    }

    @Override
    public String toString() {
        Node node = head;
        StringBuilder sb = new StringBuilder();
        while(node != null){
            sb.append(node.value).append(" ");
            node = node.next;
        }
        return sb.toString();
    }
}
