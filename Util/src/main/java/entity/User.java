package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author zxs
 * @time 2021-07-21-17:20
 */

@Data
@NoArgsConstructor
@ToString
public class User {
    private int id;
    private String name;
    private long count;
    private String space;
}
