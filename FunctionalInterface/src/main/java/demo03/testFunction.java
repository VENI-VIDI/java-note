package demo03;

import java.util.function.Function;

public class testFunction {
    public static void main(String[] args) {
        String str = "516";
        testFunctions(str,st->Integer.valueOf(st)/2,st->Integer.toString(st));
    }

    private static void testFunctions(String str, Function<String,Integer> function1,Function<Integer,String> function2){
        String ss = function1.andThen(function2).apply(str);
        System.out.println(ss+" "+ss.length());
    }
}
