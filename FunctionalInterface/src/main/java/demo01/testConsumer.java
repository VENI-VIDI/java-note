package demo01;

import java.util.Arrays;
import java.util.function.Consumer;

public class testConsumer {
    public static void main(String[] args) {
        int[] arr = {5,3,7,9,4,1};
        sort(arr,(int[] num)->{
            Arrays.sort(num);
            for(int i:num){
                System.out.print(i+" ");
            }
        });
    }

    private static void sort(int[] arr,Consumer<int[]> consumer){
        consumer.accept(arr);
    }
}
