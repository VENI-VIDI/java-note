package B_S;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class server {
    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(8888);

        while (true) {
            Socket socket = server.accept();
            new Thread(new Runnable() {
                @Override
                public void run() {

                    try {
                        InputStream is = socket.getInputStream();

                        /*将网络输入流转换为字符缓冲数据流*/
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        /*读取第一行数据*/
                        String line = br.readLine();
                        /*提取地址信息*/
                        String[] str = line.split(" ");
                        /*去掉首位 / */
                        String htmlPath = str[1].substring(1);

                        /*构造本地字节输入流*/
                        FileInputStream fis = new FileInputStream(htmlPath);

                        OutputStream os = socket.getOutputStream();

                        /*写入http协议响应开头*/
                        os.write("HTTP/1.1 200 OK\r\n".getBytes());
                        os.write("Content-Type:text/html\r\n".getBytes());
                        /*必须写入空行，否则不解析*/
                        os.write("\r\n".getBytes());

                        byte[] inBytes = new byte[1024];
                        int inLen = 0;
                        while ((inLen = fis.read(inBytes)) != -1) {
                            os.write(inBytes, 0, inLen);
                        }

                        fis.close();
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

//        server.close();
        }
    }
}
