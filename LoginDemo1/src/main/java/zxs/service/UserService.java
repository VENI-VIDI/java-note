package zxs.service;

import zxs.entity.User;

public interface UserService {

    User selectByName(String name);

}
