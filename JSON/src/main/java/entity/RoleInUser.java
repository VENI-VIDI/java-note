package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleInUser {
    private long role_id;
    private int role_level;
    private String user_name;
    private String action;
}
