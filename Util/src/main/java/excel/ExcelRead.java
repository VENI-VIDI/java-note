package excel;

import entity.User;
import org.apache.poi.hssf.eventusermodel.HSSFRequest;
import org.apache.poi.hssf.record.*;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.management.ObjectName;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author zxs
 * @time 2021-07-21-17:04
 */

public class ExcelRead {

    public static void main(String[] args) throws Exception {
//        List<Object> list = read_excel_data("D:/test.xlsx");
//        for(Object obj : list){
//            System.out.println(((User)obj).toString());
//        }
        long start = System.currentTimeMillis();
        List<Object> lis = excel_to_db("D:/write.xlsx");
        System.out.println(System.currentTimeMillis() - start);
        System.out.println(lis.size());
    }

    private static List<Object> read_excel_data(String file_path) throws Exception {
        File f = new File(file_path);
        InputStream is = null;
        Workbook wb = null;
        Class cls = null;
        Row row = null;
        Cell cell = null;
        Object obj = null;
        List<Object> list = new ArrayList<>();
        try {
            is = new FileInputStream(f.getAbsoluteFile());
            wb = WorkbookFactory.create(is); // 这种方式完美支持Excel、2003/2007/2010
            Sheet sheet = wb.getSheetAt(0);// 获取工作区（只提取第一个工作区）
            int rowCount = sheet.getPhysicalNumberOfRows();// 文件的总行数
            /**
             * 根据sheet页的名字，创建对象承接数据
             */
            String className = sheet.getSheetName();
            Class.forName("entity."+className);
            Row column = sheet.getRow(0);// 获得首行表示，单元格的内容必须与pojo类的字段名一致
            int columnNum = column.getPhysicalNumberOfCells();
//            System.out.println(columnNum);
            /**
             * 获取类的所有属性
             */
            Field[] clsField = cls.getDeclaredFields();
            List<String> fieldNames = new ArrayList<>();
            int index = 0;
            Field[] fields = new Field[columnNum];
            for(int i = 0; i < clsField.length; i++){
                fieldNames.add(clsField[i].getName());
            }
            /**
             * 排列属性数组，使得其与excel的列相对应减少后续循环查找
             */
            for(int i = 0; i < columnNum; i++){
                fields[i] = clsField[fieldNames.indexOf(column.getCell(i).getStringCellValue())];
            }
            /**
             * 逐行读取
             */
            for (int i = 1; i < rowCount; i++) {
                row = sheet.getRow(i);
                obj = cls.getConstructor().newInstance();
                for (int j = 0; j < column.getPhysicalNumberOfCells(); j++) {
                    cell = row.getCell(j);
                    fields[j].setAccessible(true);
                    Class type = fields[j].getType();
                    if (cell != null) {
                        cell.setCellType(CellType.STRING);
                        String value = cell.getStringCellValue().trim();
                        Object key = null;
                        if(type == int.class){
                            key = Integer.valueOf(value);
                        }else if(type == long.class){
                            key = Long.valueOf(value);
                        }else if(type == float.class){
                            key = Float.valueOf(value);
                        }else{
                            key = value;
                        }
                        fields[j].set(obj,key);
                    }
                }
                list.add(obj);
//                if(i % 1000 == 0) System.out.println(i);
            }
        } catch (InvalidFormatException | IOException | IllegalAccessException | InstantiationException e) {
            throw new Exception(e.getMessage());
        }finally {
            if (wb != null) {
                wb.close();
            }
            if (is != null) {
                is.close();
            }
        }
        return list;
    }

    /**
     * 将excel表格读取至集合中。
     * @param filePath
     * @return
     * @throws IOException
     */
    private static List<Object> excel_to_db(String filePath) throws IOException {
        InputStream fis = null;
        Sheet sheet = null;
//        XSSFWorkbook workbook = null;
//        XSSFWorkbook work = null;
//        SXSSFWorkbook workbook = null;
        Row row = null;
        Cell cell = null;
        List<Object> list = new ArrayList<>();
        List<Object> lis = null;
        try {
            fis = new FileInputStream(filePath);
//            workbook = new XSSFWorkbook(fis);
//            work = new XSSFWorkbook(fis);
//            workbook = new SXSSFWorkbook(work);
            sheet = WorkbookFactory.create(fis).getSheetAt(0);
//            sheet = workbook.getSheetAt(0);
            int count = sheet.getPhysicalNumberOfRows();
            Row columns = sheet.getRow(0);
            Row typeColumn = sheet.getRow(1);
            int columnCount = columns.getPhysicalNumberOfCells();
            List<String> columnNames = new ArrayList<>();
            String[] type = new String[columnCount];
            for(int i = 0; i < columnCount; i++){
                cell = columns.getCell(i);
                cell.setCellType(CellType.STRING);
                String value = cell.getStringCellValue();
                if(value == null || "".equals(value.trim())){
                    return null;
                }else{
                    columnNames.add(value.trim());
                }
            }
            list.add(columnNames);
            for(int i = 0; i < columnCount; i++){
                cell = typeColumn.getCell(i);
                cell.setCellType(CellType.STRING);
                String value = cell.getStringCellValue();
                if(value == null || "".equals(value.trim())){
                    return null;
                }else{
                    type[i] = value.trim();
                }
            }
            for(int i = 2; i < count; i++){
                row = sheet.getRow(i);
                lis = new ArrayList<>();
                for(int j = 0; j < columnCount; j++){
                    cell = row.getCell(j);
                    if(cell == null){
                        if("string".equals(type[j])) {
                            lis.add("");
                        }else{
                            lis = null;
                            break;
                        }
                    }else {
                        cell.setCellType(CellType.STRING);
                        String value = cell.getStringCellValue().trim();
                        if ("int".equals(type[j])) {
                            lis.add(Integer.valueOf(value));
                        } else if ("float".equals(type[j])) {
                            lis.add(Float.valueOf(value));
                        } else if ("long".equals(type[j])) {
                            lis.add(Long.valueOf(value));
                        } else {
                            lis.add(value);
                        }
                    }
                }
                list.add(lis);
            }
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return list;
    }

}
