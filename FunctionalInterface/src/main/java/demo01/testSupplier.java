package demo01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Supplier;

public class testSupplier {
    public static void main(String[] args) {
        int[] arr = {1,21,43,11,27,55};
        int[] res = sum(()->{
            Arrays.sort(arr);
            return arr;
        });
        for(int i:res) System.out.print(i+",");
    }

    private static int[] sum(Supplier<int[]> supplier){
        return supplier.get();
    }
}
