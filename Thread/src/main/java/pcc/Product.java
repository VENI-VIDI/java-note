package pcc;

public class Product implements Runnable{

    private Clerk clerk;

    public Clerk getClerk() {
        return clerk;
    }

    public void setClerk(Clerk clerk) {
        this.clerk = clerk;
    }

    @Override
    public void run() {
        while(true){
            synchronized(clerk){
                int x =clerk.getCount();
                if(x < 20){
                    notify();
                    this.clerk.setCount(x+1);
                    System.out.println(Thread.currentThread().getName()+":"+x+1);
                }else{
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
