package runnable;

public class MyThreadTest {
    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        Thread thread1 = new Thread(myThread);
        Thread thread2 = new Thread(myThread);
        Thread thread3 = new Thread(myThread);
        thread1.setName("1");
        thread2.setName("2");
        thread3.setName("3");

        thread1.start();
        thread2.start();
        thread3.start();

        for (int i = 0; i < 100; i++) {
            System.out.println(i);
        }
    }
}

class MyThread implements Runnable {

    private int target = 100;
    Object obj = new Object();

    @Override
    public void run() {

        while (true) {
            synchronized (obj) {
                if(target >0 ) {
//                try {
//                    Thread.sleep(100);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
                    System.out.println(Thread.currentThread().getName() + "余票:" + target);
                    target--;
                }else{
                    break;
                }
            }
        }
    }
}
