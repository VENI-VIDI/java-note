import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.Map;
import java.util.Set;

public class Demo1 {

    @Test
    public void test1(){
//        1、获取连接
        Jedis jedis = new Jedis("localhost",6379);
//        2、操作

        /*1、String*/
        jedis.set("Into","yxl");
        /*指定时效*/
        jedis.setex("IntoTime",147,"joker");
        String into = jedis.get("Into");

        /*2、HashMap*/
        jedis.hset("family","zxs","yxl");
        /*获取*/
        String hget = jedis.hget("family", "zxs");
        Map<String, String> family = jedis.hgetAll("family");
        /*获取键集合*/
        Set<String> strings = family.keySet();
        /*遍历*/
        for(String str:strings){
            family.get(str);
        }


        /*3、list*/
        /*左添加*/
        jedis.lpush("like","zxs","yxl");
        /*右添加*/
        jedis.rpush("love","zxs","yxl");
        /*左删除       自动返回一个值*/
        String like = jedis.lpop("like");
        /*右删除       自动返回一个值*/
        String love = jedis.rpop("love");

        /*4、set*/
        /*5、*/


//        3、关闭连接
        jedis.close();
    }
}
