package list;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zxs
 * @time 2021-07-07-9:33
 */

public class MyTest {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("asa");
        list.add("asdasda");
        System.out.println(list);
        list = new ArrayList<>();
        System.out.println(list);
    }

    /**
     *
     */
    @Test
    public void testMyList(){
        MyList list = new MyList();
        list.add(4).add(1).add(7).add(8).add(33).add(82).add(33);
        System.out.println(list.size());
        System.out.println(list.toString());
        System.out.println(list.contains(33));
        System.out.println(list.removeAll(33));
        System.out.println(list.size());
        System.out.println(list.update(8,6));
        System.out.println(list.toString());
    }
}
