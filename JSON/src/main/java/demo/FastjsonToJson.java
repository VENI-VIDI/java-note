package demo;


import com.alibaba.fastjson.JSONObject;
import entity.Command;
import entity.Gui;
import entity.Role;

import java.util.ArrayList;

public class FastjsonToJson {
    public static void main(String[] args) {
//        String jsonStr = "{\"role_id\":1,\"role_name\":\"A\",\"guis\":[{\"role_id\":1,\"gui_name\":\"a\",\"ori\":1,\"commands\":[{\"role_id\":1,\"gui_name\":\"a\",\"command_name\":\"aA\",\"ui_name\":\"a@aA\"}]}]}";
//        Role role = JSONObject.parseObject(jsonStr, Role.class);
//        System.out.println(role);

        Role role = new Role();
        role.setRole_id(1L);
        role.setRole_name("A");
        role.setGuis(new ArrayList<>());

        Gui gui = new Gui();
        gui.setGui_name("a");
        gui.setRole_id(1L);
        gui.setOri(1);
        gui.setCommands(new ArrayList<>());

        Command command = new Command();
        command.setRole_id(1L);
        command.setGui_name("a");
        command.setCommand_name("aA");
        command.setUi_name(command.getGui_name() + "@" + command.getCommand_name());

        role.getGuis().add(gui);
        gui.getCommands().add(command);

        String str1 = JSONObject.toJSONString(role);
        String str2 = JSONObject.toJSONString(gui);
        String str3 = JSONObject.toJSONString(command);
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str3);
    }
}
