package session;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Properties;

public class ReflectTest {
    public static void main(String[] args) throws Exception {
        /*加载配置文件*/
        Properties properties = new Properties();
        ClassLoader classLoader = ReflectTest.class.getClassLoader();
        InputStream is = classLoader.getResourceAsStream("pro.properties");
        properties.load(is);

        /*获取文件中定义的数据*/
        String className = properties.getProperty("className");
        String methodName = properties.getProperty("methodName");

        /*加载进入内存*/
        Class cls = Class.forName(className);
        /*创建对象*/
        Object obj = cls.newInstance();
        /*获取方法*/
        Method method = cls.getMethod(methodName);
        /*执行方法*/
        method.invoke(obj);
    }
}
