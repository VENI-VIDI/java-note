package zxs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import zxs.entity.User;
import zxs.service.impl.UserServiceImpl;

@RequestMapping("/login")
@Controller
public class LoginController {

    @Autowired
    private UserServiceImpl userService;

    @RequestMapping("/user")
    public String testUserLogin(@RequestBody User user){
        System.out.println("...testUserLogin...");
        if(user.getUserPassword() == userService.selectByName(user.getUserName()).getUserPassword()) return "success";
        return "error";
    }

}
