package tree.bst;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import tree.TreeNode;

/**
 * @author zxs
 * @time 2021-07-05-9:20
 */

public class TestT {

    static long start;
    static long end;

    @Before
    public void before(){
        start = System.currentTimeMillis();
    }

    @After
    public void after(){
        end = System.currentTimeMillis();
        System.out.println(end - start);
    }

    public static void main(String[] args) {

    }

    @Test
    public void testAvlTree(){
        AvlTree tree = new AvlTree(new TreeNode(5));
        for(int i = 0; i < 1000; i++){
            tree.add((int)(Math.random()*1000*Math.PI));
        }
        tree.inOrder();
        System.out.println();
        System.out.println(tree.size());
        tree.remove(111);
        tree.inOrder();
        System.out.println(tree.size());
    }

    @Test
    public void testBinarySortTree(){
        BinarySortTree tree = new BinarySortTree(new TreeNode(5));
        for(int i = 0; i < 1000; i++){
            tree.add((int)(Math.random()*1000*Math.PI));
        }
        tree.inOrder();
        System.out.println();
        System.out.println(tree.size());
        tree.remove(111);
        tree.inOrder();
        System.out.println(tree.size());
    }

}
