package zxs.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import zxs.dao.UserDao;
import zxs.entity.User;
import zxs.service.UserService;

@Component
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User selectByName(String name) {
        return userDao.selectByName(name);
    }
}
