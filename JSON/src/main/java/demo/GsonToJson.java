package demo;

import com.google.gson.Gson;
import com.jsoniter.output.JsonStream;
import entity.Command;
import entity.Gui;
import entity.Role;
import entity.RoleInUser;
import top.jfunc.json.impl.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GsonToJson {
    public static void main(String[] args) {

        Role role = new Role();
        role.setRole_id(1L);
        role.setRole_name("A");
        role.setGuis(new ArrayList<>());

        Gui gui = new Gui();
        gui.setGui_name("a");
        gui.setRole_id(1L);
        gui.setOri(1);
        gui.setCommands(new ArrayList<>());

        Command command = new Command();
        command.setRole_id(1L);
        command.setGui_name("a");
        command.setCommand_name("aA");
        command.setUi_name(command.getGui_name() + "@" + command.getCommand_name());

        gui.getCommands().add(command);
        role.getGuis().add(gui);

        com.google.gson.Gson gson = new com.google.gson.Gson();

        System.out.println(gson.toJson(role));

        System.out.println(gson.fromJson("{\"role_id\":1,\"role_name\":\"A\",\"guis\":[{\"role_id\":1,\"gui_name\":\"a\",\"ori\":1,\"commands\":[{\"role_id\":1,\"gui_name\":\"a\",\"command_name\":\"aA\",\"ui_name\":\"a@aA\"}]}]}",Role.class));
        System.out.println(gson.fromJson("{\"role_id\":1,\"role_name\":\"A\",\"guis\":[{\"role_id\":1,\"gui_name\":\"a\",\"ori\":1,\"commands\":[{\"role_id\":1,\"gui_name\":\"a\",\"command_name\":\"aA\",\"ui_name\":\"a@aA\"}]}]}",role.getClass()));


        RoleInUser roleInUser = new RoleInUser(233L,3,"zxs","I");
        List<RoleInUser> list = new ArrayList<>();
        list.add(roleInUser);
        System.out.println(JsonStream.serialize(list));
        // "[{\"role_id\":233,\"role_level\":3,\"user_name\":\"zxs\",\"action\":\"I\"}]"

    }
}
