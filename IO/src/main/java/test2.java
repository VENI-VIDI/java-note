import java.io.*;

/**
 * @author zxs
 * @time 2021-08-05-11:17
 */

public class test2 {
    public static void main(String[] args) throws IOException {
        BufferedInputStream fis = new BufferedInputStream(new FileInputStream("D:\\zxsProject\\java-note\\IO\\src\\start.xlsx"));
        BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream("D:\\zxsProject\\java-note\\IO\\src\\end.xlsx"));
//        HashMap<String,String> map = new HashMap<>();


        byte[] bytes = new byte[1024];
        int len = 0;
        while((len = fis.read(bytes)) != -1){
            fos.write(bytes);
        }
        fos.close();
        fis.close();
    }
}
